/* 
 * This exercise is adapted from HTML 5 Programmierung von Kopf bis Fuß, (2012), Freeman, Robson, chap. 5
 */



var watcherId = null;
var map = null;
var ourCoordinates = {
        latitude: 47.624851,
        longitude: -122.52099
};

window.onload = checkPosition;


function checkPosition()
{
    // is geo navigation supported?
    if (navigator.geolocation) {
        // in case of geolocation enabled, run "showPosition()" as handler
 //       navigator.geolocation.getCurrentPosition(showPosition, showError);
        var watchButton = document.getElementById("watch");
        watchButton.onclick = watchPosition;
        var stopButton = document.getElementById("stop");
        stopButton.onclick = stop;
    } 
    else {
        // in case of geolocation not enabled, show a popup
        alert ("No support for geolocation API!");
    } 
}

function watchPosition ()
{
    watcherId = navigator.geolocation.watchPosition(showPosition, showError, 
    {timeout:5000} );
}

function stop()
{
    if (watcherId) {
        navigator.geolocation.clearWatch(watcherId);
        watcherId = null;
    }
}

// showPosition displays your position within the HTML page
function showPosition (position) 
{
    
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;
    
    var div = document.getElementById("position");
    div.innerHTML= "Your positions Latitude and Longitude is " + lat + " and " + lon; 
    
    // #########################################################################
    // ADD LATITUDE AND LONGITUDE AS TEXT TO THE "POSITION" WITHIN THE HTML PAGE
    // USE INNERHTML AS YOU KNOW FROM PREVIOUS EXERCISES AND ADD A STRING TO IT

    
 
    
    var km = calculateDistance({latitude: lat, longitude: lon}, ourCoordinates);
    
	
	// CALCULATE THE DISTANCE BETWEEN "ourCoordinates" AND OUR CURRENT POSITION
    // BY COMPLETING ABOVE STATEMENT
    // #########################################################################
 
    var distance = document.getElementById("distance");
    distance.innerHTML = "Distance to WickedlySmart headquarter: " +
            km + " km";
    
    if (map === null){
        showMap(position.coords);
    } else
    {
        scrollMapToPosition(position.coords);
    }
    
}


function calculateDistance(startCoords, targetCoords)
{
    var startLatRads   = degreeInRadiant(startCoords.latitude);
    var startLongRads  = degreeInRadiant(startCoords.longitude);
    var targetLatRads  = degreeInRadiant(targetCoords.latitude);
    var targetLongRads = degreeInRadiant(targetCoords.longitude);
    
    var Radius = 6371 // radius of planet earth :-)
    
    var distance = ((Math.acos(
                            Math.sin(startLatRads) * Math.sin(targetLatRads) +
                            Math.cos(startLatRads) * Math.cos(targetLatRads) +
                            Math.cos(startLongRads - targetLongRads)
                            )) * Radius);  
    
    return distance;
}

function degreeInRadiant(degree)
{
    radiant = (degree * Math.PI) / 180;
    return radiant;
}

function showMap(coords)
{
    var googleLatitudeLongitude = 
            new google.maps.LatLng(coords.latitude, coords.longitude);
    
    var mapOptions = {
        zoom : 10,
        center : googleLatitudeLongitude,
        mapTypeId : google.maps.MapTypeId.ROADMAP
    };
    
    var mapDiv = document.getElementById("map");
    map = new google.maps.Map(mapDiv,mapOptions);
    
    var title = "Your position";
    var content = "You are here: "+coords.latitude+", "+coords.longitude;
    addMarker(map,googleLatitudeLongitude, title, content);
}



function showError(error)
// the showError handler will receive an error object in case it is called

{
    // The error object has an attribute called "code" which
    // contains values from 0 to 3
    // the errorTypes object below maps a errorType to each code so
    // that we can display more meaningful messages.

    var errorTypes = {
        0: "unknown error",
        1: "no user authorization to use gelocation",
        2: "position cannot be identified",
        3: "time out"
    };
    
    var errorMsg = errorTypes[error.code]; // identify the correct errorType base on the code
    // and put it into errorMsg
    
    // in case of error 0 or 2, the attribute message may contain helpful additional information
    // thus we extend the string value by this message.
    if(error.code === 0 || error.code === 2) {
        errorMsg = errorMsg + " " + error.message;
    }
    
    var div = document.getElementById("position");
    div.innerHTML = errorMsg;
}

function addMarker(map, latlong, title, content) {
	var markerOptionen = {
		position: latlong,
		map: map,
		title: title,
		clickable: true
	};
	var marker = new google.maps.Marker(markerOptionen);

	var infoWindowOptions = {
		content: content,
		position: latlong
	};

	var infoWindow = new google.maps.InfoWindow(infoWindowOptions);

	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.open(map);
	});
}

function scrollMapToPosition(coords)
{
    var lat = coords.latitude;
    var lon = coords.longitude;
    var latLong = new google.maps.LatLng(lat, lon);
    map.panTo(latLong);
    addMarker(map, latLong, "Your new position", "You are now at: "+lat+", "+ lon);
}